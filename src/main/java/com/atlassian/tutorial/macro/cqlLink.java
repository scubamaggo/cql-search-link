package com.atlassian.tutorial.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Scanned
public class cqlLink implements Macro {

    private PageBuilderService pageBuilderService;
    private CQLSearchService cqlSearchService;

    @Autowired
    public cqlLink(@ComponentImport PageBuilderService pageBuilderService,
                   @ComponentImport CQLSearchService cqlSearchService) {
        this.pageBuilderService = pageBuilderService;
        this.cqlSearchService = cqlSearchService;
    }

    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {


        pageBuilderService.assembler().resources().requireWebResource("de.digitec.confluence.plugins.cql-search-link:cql-search-link-resources");

        cqlSearchService.searchContent(map.get("CQL"));

        String output = "<div class =\"helloworld\">";

        if (map.get("Name") != null) {
            output = output + ("<h1>Hello " + map.get("Name") + "!</h1>");
        } else {
            output = output + "<h1>Hello World!<h1>";
        }

        output = output + "</div>" + "</div>";

        return output;
    }

    public BodyType getBodyType() { return BodyType.NONE; }

    public OutputType getOutputType() { return OutputType.BLOCK; }
}
